package org.oak.uml.algorithm.util;

import org.oak.uml.algorithm.model.Point;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class WeightUtil {

    public static List<Point> applyWeight(List<Point> points, double... multipliers) {
        return points.stream().map(p -> new Point(p.getDescription(), multiply(p.getCoordinates(), multipliers, false))).collect(Collectors.toList());
    }

    public static List<Point> unApplyWeight(List<Point> points, double... multipliers) {
        return points.stream().map(p -> new Point(p.getDescription(), multiply(p.getCoordinates(), multipliers, true))).collect(Collectors.toList());
    }

    private static double[] multiply(double[] coordinates, double[] multipliers, boolean reverse) {
        double[] newCoordinates = new double[coordinates.length];
        for (int i = 0; i < coordinates.length; i++) {
            newCoordinates[i] = reverse ?  coordinates[i] / multipliers[i] : coordinates[i] * multipliers[i];
        }
        return newCoordinates;
    }
}
