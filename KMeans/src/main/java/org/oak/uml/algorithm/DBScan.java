package org.oak.uml.algorithm;

import org.oak.uml.algorithm.model.Cluster;
import org.oak.uml.algorithm.model.Point;
import org.oak.uml.algorithm.util.DistanceMeasure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class DBScan implements ClusteringMethod {

    private final List<Point> points;
    private final DistanceMeasure measure;

    private double maxDistance;
    private int minDensity;
    private boolean randomize;

    public DBScan(List<Point> points, DistanceMeasure measure) {
        this(points, measure, 1.0, 2, false);
    }

    public DBScan(List<Point> points, DistanceMeasure measure, double maxDistance, int minDensity, boolean randomize) {
        this.points = points;
        this.measure = measure;
        this.maxDistance = maxDistance;
        this.minDensity = minDensity;
        this.randomize = randomize;
    }

    public void setMaxDistance(double maxDistance) {
        this.maxDistance = maxDistance;
    }

    public void setMinDensity(int minDensity) {
        this.minDensity = minDensity;
    }

    public void setRandomize(boolean randomize) {
        this.randomize = randomize;
    }

    @Override
    public List<Cluster> clusters() {
        if (randomize) {
            Collections.shuffle(points);
        }

        Set<Point> visited = new HashSet<>();
        List<Cluster> clusters = new ArrayList<>();

        points.forEach(p -> {
            if (!visited.contains(p)) {
                visited.add(p);
                List<Point> neighbours = findNeighbours(p);

                if (neighbours.size() >= minDensity) {
                    for (int i = 0; i < neighbours.size(); ++i) {
                        Point np = neighbours.get(i);
                        if (!visited.contains(np)) {
                            visited.add(np);
                            List<Point> nNeighbours = findNeighbours(np);
                            if (nNeighbours.size() >= minDensity) {
                                mergeCollections(neighbours, nNeighbours);
                            }
                        }
                    }
                    clusters.add(new Cluster(neighbours));
                }
            }
        });

        return clusters;
    }

    private void mergeCollections(List<Point> c1, List<Point> c2) {
        c1.addAll(c2.stream().filter(p -> !c1.contains(p)).collect(Collectors.toList()));
    }

    private List<Point> findNeighbours(Point p) {
        return points.parallelStream().filter(np -> measure.distance(p, np) < maxDistance).collect(toList());
    }
}
