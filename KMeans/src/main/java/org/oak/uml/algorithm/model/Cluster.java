package org.oak.uml.algorithm.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Cluster {
    private final Set<Point> points;
    private Point centroid;

    public Cluster() {
        this.points = new HashSet<>();
    }

    public Cluster(Collection<Point> points) {
        this();
        this.points.addAll(points);
        calcCentroid();
    }

    public Set<Point> getPoints() {
        return points;
    }

    public Point getCentroid() {
        return centroid;
    }

    public void addPoint(Point p) {
        if (points.add(p)) {
            calcCentroid();
        }
    }

    public void deletePoint(Point p) {
        if (points.remove(p)) {
            calcCentroid();
        }
    }

    public String buildDescription() {
        return points.size() + "; " + points.stream().map(Point::getDescription).collect(Collectors.joining(","));
    }

    @Override
    public String toString() {
        return "{" +
                "points=" + points +
                ", size=" + points.size() +
                ", centroid=" + centroid +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cluster cluster = (Cluster) o;
        return Objects.equals(points, cluster.points) &&
                Objects.equals(centroid, cluster.centroid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(points, centroid);
    }

    private void calcCentroid() {
        double[] center = points.stream().map(Point::getCoordinates).reduce(null, (a, b) -> {
            if (a == null) {
                return b;
            } else if (b == null) {
                return a;
            } else {
                double[] c = new double[a.length];
                for (int i = 0; i < a.length; i++) {
                    c[i] = a[i] + b[i];
                }
                return c;
            }
        });
        center = (center != null) ? Arrays.stream(center).map(i -> i / points.size()).toArray() : null;
        centroid = new Point("Centroid", center);
    }
}
