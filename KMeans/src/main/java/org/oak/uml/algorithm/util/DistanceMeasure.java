package org.oak.uml.algorithm.util;

import org.oak.uml.algorithm.model.Cluster;
import org.oak.uml.algorithm.model.Point;

import java.util.List;

@FunctionalInterface
public interface DistanceMeasure {

    double distance(Point p1, Point p2);

    default Cluster findClosest(Point p, List<Cluster> clusters) {
        double minDistance = Double.POSITIVE_INFINITY, currentDistance = 0;
        Cluster closest = null;
        for (Cluster cluster : clusters) {
            currentDistance = distance(p, cluster.getCentroid());
            if (currentDistance < minDistance) {
                minDistance = currentDistance;
                closest = cluster;
            }
        }
        return closest;
    }
}
