package org.oak.uml.algorithm.util;

import java.awt.Color;
import java.util.List;

import javax.swing.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.oak.uml.algorithm.model.Cluster;

import static org.jfree.chart.plot.PlotOrientation.VERTICAL;

public class DrawUtil extends JFrame {
    private static final long serialVersionUID = 6294689542092367723L;

    private DrawUtil(List<Cluster> clusters, String title, int x, int y, String xTitle, String yTitle) {
        super(title);

        XYSeriesCollection dataset = new XYSeriesCollection();
        clusters.forEach(c -> {
            XYSeries series = new XYSeries(c.buildDescription());
            c.getPoints().forEach(p -> series.add(p.getCoordinates()[x], p.getCoordinates()[y]));
            dataset.addSeries(series);
        });

        JFreeChart chart = ChartFactory.createScatterPlot("", xTitle, yTitle, dataset, VERTICAL, false, true, false);
        chart.getPlot().setBackgroundPaint(Color.WHITE);
        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);
    }

    public static void draw2D(List<Cluster> clusters) {
        draw2D(clusters, "Clusters plot");
    }

    public static void draw2D(List<Cluster> clusters, String title) {
        draw2D(clusters, title, 0, 1, "X-Axis", "Y-Axis");
    }

    public static void draw2D(List<Cluster> clusters, String title, int x, int y, String xTitle, String yTitle) {
        SwingUtilities.invokeLater(() -> {
            DrawUtil example = new DrawUtil(clusters, title, x, y, xTitle, yTitle);
            example.setSize(600, 600);
            example.setLocationRelativeTo(null);
            example.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            example.setVisible(true);
        });
    }
}
