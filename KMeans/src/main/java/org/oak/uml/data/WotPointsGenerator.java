package org.oak.uml.data;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.oak.uml.algorithm.model.Point;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.util.stream.Collectors.*;

public class WotPointsGenerator {

    // TODO: check root path
    private static final String URLS_PATH = "./KMeans/src/main/resources/tanks-urls.csv";
    private static final String RAW_DATA_PATH = "./KMeans/src/main/resources/tanks-data.csv";
    private static final String TANKS_DATA_PATH = "./KMeans/src/main/resources/tanks-points.csv";

    public static List<Point> generatePoints(boolean readFromFile, boolean saveToFile) throws IOException {
        List<Point> tanksPoints;

        if (readFromFile) {
            tanksPoints = readTanksFromFile();
        } else {
            List<String> tanksUrls = getAllTanks();
            List<Tank> tanksData = tanksUrls.parallelStream().map(WotPointsGenerator::getTankInfo).filter(Objects::nonNull).collect(toList());
            tanksPoints = tanksData.parallelStream().map(WotPointsGenerator::tankToPoint).collect(toList());

            if (saveToFile) {
                Files.write(Paths.get(URLS_PATH), tanksUrls.toString().getBytes(),
                        WRITE, CREATE, TRUNCATE_EXISTING);
                Files.write(Paths.get(RAW_DATA_PATH), tanksData.toString().getBytes(),
                        WRITE, CREATE, TRUNCATE_EXISTING);
            }
        }

        if (saveToFile) {
            Files.write(Paths.get(TANKS_DATA_PATH), tanksPoints.toString().getBytes(),
                    WRITE, CREATE, TRUNCATE_EXISTING);
        }

        return tanksPoints;
    }

    private static List<Point> readTanksFromFile() throws IOException {
        List<String> tanksInfo = getAllTanksFromFile();
        List<Tank> tanksData = tanksInfo.parallelStream().map(WotPointsGenerator::getTankInfoFromFile).collect(toList());

        return tanksData.parallelStream().map(WotPointsGenerator::tankToPoint).collect(toList());
    }

    private static List<String> getAllTanks() throws IOException {
        String BASE_URL = "https://tank-compare.com";
        return Jsoup.connect(BASE_URL + "/ru/tank-list").get().body().getElementsByClass("tank-list").stream()
                .flatMap(e -> e.getElementsByTag("li").stream())
                .map(e -> BASE_URL + e.getElementsByTag("a").get(0).attributes().get("href"))
                .collect(toList());
    }

    private static List<String> getAllTanksFromFile() throws IOException {
        String allInfo = String.valueOf(Files.readAllLines(Paths.get(TANKS_DATA_PATH)));
        return Arrays.asList(allInfo.split("},"));
    }

    private static Tank getTankInfo(String url) {
        try {
            String[] urlParts = url.split("/");

            Document doc = Jsoup.connect(url).get();
            Elements mainData = doc.body().getElementById("docs-start-here").parent().getElementsByTag("tr");
            Elements gunData = doc.body().getElementById("guns").getElementsByTag("tr");

            String nation = urlParts[5];
            String type = urlParts[6];
            String vehicle = urlParts[7];
            double lvl = parseLvl(mainData.get(0).text());
            double hp = getLastNumber(mainData.get(1).text());
            double maxSpeed = getLastNumber(mainData.get(4).text());
            double maxSpeedBack = getLastNumber(mainData.get(5).text());
            double coolDown = getLastNumber(gunData.get(3).text());
            double aimTime = getLastNumber(gunData.get(5).text());
            double accuracy = getLastNumber(gunData.get(6).text());
            double damage = getLastNumber(gunData.get(9).getElementsByTag("dd").get(0).text());

            return new Tank(nation, type, vehicle, lvl, hp, maxSpeed, maxSpeedBack, coolDown, aimTime, accuracy, damage);
        } catch (Exception e) {
            System.err.println("Exception occurred while parsing data for " + url + ", skipping");
            e.printStackTrace();
            return null;
        }
    }

    private static Tank getTankInfoFromFile(String info) {
        String[] infoPath = info.split("', ");
        String[] description = infoPath[0].substring(info.indexOf('\'') + 1).split(":");
        String[] coordinates = infoPath[1].substring(infoPath[1].indexOf('[') + 1, infoPath[1].indexOf(']')).split(", ");

        String nation = description[0];
        String type = description[1];
        String vehicle = description[2];
        double lvl = Double.parseDouble(coordinates[0]);
        double hp = Double.parseDouble(coordinates[1]);
        double maxSpeed = Double.parseDouble(coordinates[2]);
        double maxSpeedBack = Double.parseDouble(coordinates[3]);
        double coolDown = Double.parseDouble(coordinates[4]);
        double aimTime = Double.parseDouble(coordinates[5]);
        double accuracy = Double.parseDouble(coordinates[6]);
        double damage = Double.parseDouble(coordinates[7]);

        return new Tank(nation, type, vehicle, lvl, hp, maxSpeed, maxSpeedBack, coolDown, aimTime, accuracy, damage);
    }

    private static double parseLvl(String s) {
        String n = s.split(" ")[1];
        switch (n) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            case "VI":
                return 6;
            case "VII":
                return 7;
            case "VIII":
                return 8;
            case "IX":
                return 9;
            case "X":
                return 10;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static double getLastNumber(String s) {
        String[] parts = s.split(" ");
        return Double.parseDouble(parts[parts.length - 2]);
    }

    private static Point tankToPoint(Tank tank) {
        String description = tank.getNation() + ":" + tank.getType() + ":" + tank.getVehicle();
        return new Point(description, tank.getLvl(), tank.getHp(), tank.getMaxSpeed(), tank.getMaxSpeedBack(),
                tank.getCoolDown(), tank.getAimTime(), tank.getAccuracy(), tank.getDamage());
    }
}