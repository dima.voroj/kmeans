package org.oak.uml;

import org.oak.uml.algorithm.ClusteringMethod;
import org.oak.uml.algorithm.DBScan;
import org.oak.uml.algorithm.KMeans;
import org.oak.uml.algorithm.model.Cluster;
import org.oak.uml.algorithm.model.Point;
import org.oak.uml.algorithm.util.DistanceMeasure;
import org.oak.uml.algorithm.util.DrawUtil;
import org.oak.uml.algorithm.util.NormUtil;
import org.oak.uml.algorithm.util.TankClusterPrintUtil;
import org.oak.uml.algorithm.util.WeightUtil;
import org.oak.uml.data.WotPointsGenerator;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        List<Point> initPoints = WotPointsGenerator.generatePoints(true, false);
        List<Point> normPoints = NormUtil.minMaxNorm(initPoints);
        List<Point> weightPoints = WeightUtil.applyWeight(normPoints, 1, 1.2, 1.2, 0.9, 1.1, 1.2, 1.3, 1.5);

        DistanceMeasure euclidMeasure = (p1, p2) -> {
            if (p1.getCoordinates().length == p2.getCoordinates().length) {
                double squaresSum = 0, coordinatesDiff;
                for (int i = 0; i < p1.getCoordinates().length; i++) {
                    coordinatesDiff = p2.getCoordinates()[i] - p1.getCoordinates()[i];
                    squaresSum += coordinatesDiff * coordinatesDiff;
                }
                return Math.sqrt(squaresSum);
            } else {
                throw new IllegalArgumentException();
            }
        };
        DistanceMeasure manhattanMeasure = (p1, p2) -> {
            if (p1.getCoordinates().length == p2.getCoordinates().length) {
                double sum = 0;
                for (int i = 0; i < p1.getCoordinates().length; i++) {
                    sum += Math.abs(p1.getCoordinates()[i] - p2.getCoordinates()[i]);
                }
                return sum;
            } else {
                throw new IllegalArgumentException();
            }
        };

        ClusteringMethod kMeans = new KMeans(weightPoints, euclidMeasure, 20, 200, true);
        ClusteringMethod dbScan = new DBScan(weightPoints, euclidMeasure, 0.15, 3, true);

        showClusters(kMeans.clusters(), "KMeans clusters");
        showClusters(dbScan.clusters(), "DBScan clusters");
    }

    private static void showClusters(List<Cluster> clusters, String title) {
        TankClusterPrintUtil.printClusters(clusters);
        DrawUtil.draw2D(clusters, title, 0, 1, "Level", "HP");
        DrawUtil.draw2D(clusters, title, 1, 7, "HP", "Damage");
        DrawUtil.draw2D(clusters, title, 5, 7, "AIM", "Damage");
    }
}
